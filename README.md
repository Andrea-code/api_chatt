# API for Chatt App

**This API REST was made for the app [Chatt](https://gitlab.com/Andrea-code/chatt-app). Is a very simple API REST.**

Tecnologies was used:
*  Java 8
*  Spring
*  Firebase Cloud Firestore

The API allows:
* Create users
* Gell all users from aplication
* Create General (Public) and Private (1 to 1) messages.
this is the structure of Chatt collection: 
![chatt_pp](/uploads/403cdc0d6eab5436ac50db99eb32836e/chatt_pp.png)
* Gell all conversations from current user has

All this information is save on Firebase Cloud Firestore,
to have well structured data and understand them easily.

**IMPORTANT** 
For security reasons, the file *serviceAccountKey.json* and the URL of the Database have been deleted.
Please add the file and url with your own Firebase project.