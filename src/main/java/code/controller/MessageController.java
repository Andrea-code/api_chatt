package code.controller;

import code.firebase.FirebaseSnippets;
import code.model.Message;
import code.model.User;
import com.google.api.core.ApiFuture;
import com.google.cloud.Timestamp;
import com.google.cloud.firestore.*;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@RestController
public class MessageController {

    private Firestore db = FirebaseSnippets.db;
    private Message message = new Message();
    private List<QueryDocumentSnapshot> documents;

    //No realtime messages
    @GetMapping("/messages/{iudSender}/{uidReceiver}")
    public List<Message> getMessages(@PathVariable String iudSender, @PathVariable String uidReceiver) throws ExecutionException, InterruptedException {

        CollectionReference colRef = db.collection("chat")
                .document(iudSender)
                .collection(uidReceiver);

        List<QueryDocumentSnapshot> documents = colRef.get().get().getDocuments();

        List<Message> messages = new ArrayList<>();

        for (QueryDocumentSnapshot document : documents) {
            Message m = new Message();
            if(document.getData().get("content") != null){
                m.setContent(document.getData().get("content").toString());
            }

            if(document.getData().get("url_photo") != null){
                m.setUrl_photo(document.getData().get("url_photo").toString());
            }

            m.setSender_id(document.getData().get("sender_id").toString());
            m.setReceiver_id(document.getData().get("receiver_id").toString());
            //m.setCreation_date(Timestamp.now(document.getData().get("date")));

            messages.add(m);
        }

        return messages;
    }


    /**
     * This method actually is useless in the app. Only is for test purposes.
     * In case is been used, the chat divides in two:
     * general (public) and private (1 to 1)
     *
     * @param message
     * @throws ExecutionException
     * @throws InterruptedException
     */
    @PostMapping("/message/general")
    public void newMessageGeneralChat(@RequestBody Message message) throws ExecutionException, InterruptedException {
        Map<String, Object> data = new HashMap<>();
        data.put("user_name", message.getUser_name());
        data.put("content", message.getContent());
        data.put("url_photo", message.getUrl_photo());
        data.put("sender_id", message.getSender_id());
        data.put("date", message.getCreation_date());

        ApiFuture<DocumentReference> docGeneral = db.collection("chat")
                .document("general")
                .collection(message.getSender_id()).add(data);
    }


    @GetMapping("/messages/{id}")
    public List<User> getConvertationsById(@PathVariable String id) throws ExecutionException, InterruptedException {
        Iterable<CollectionReference> collections = db.collection("chat")
                .document(id).getCollections();

        List<String> colId = new ArrayList<>();
        for (CollectionReference collection: collections) {
            colId.add(collection.getId());
        }

        //Get all users in our aplication
        ApiFuture<QuerySnapshot> dbUsers = db.collection("users").get();
        List<QueryDocumentSnapshot> documentsUsers = dbUsers.get().getDocuments();


        List<User> users = new ArrayList<>();
        Boolean find = false;

        for (QueryDocumentSnapshot document : documentsUsers){
            for(String uid : colId){
                if(uid.equals( document.getData().get("uid").toString() )){
                    users.add(  document.toObject(User.class) );
                }
            }
        }

        return users;

    }

    @PostMapping("/message")
    public void newMessagePrivateChat(@RequestBody Message message) {

        Map<String, Object> data = new HashMap<>();
        data.put("date", Timestamp.now());
        data.put("content", message.getContent());
        System.out.println("url_photo" + message.getUrl_photo());
        data.put("url_photo", message.getUrl_photo());
        data.put("sender_id", message.getSender_id());
        data.put("receiver_id", message.getReceiver_id());


        ApiFuture<DocumentReference> docSender = db.collection("chat")
                .document(message.getSender_id())
                .collection(message.getReceiver_id())
                .add(data);

        ApiFuture<DocumentReference> docReceiver = db.collection("chat")
                .document(message.getReceiver_id())
                .collection(message.getSender_id())
                .add(data);

        /*
        * [!] In case is been used this method with newMessageGeneralChat() method *
        *           uncomment this  code and replace from upside code              *
        *                                                                          *
                DocumentReference docSender = db.collection("chat")
                        .document("private")
                        .collection(message.getSender_id())
                        .document(message.getReceiver_id());

                DocumentReference docReceiver = db.collection("chat")
                        .document("private")
                        .collection(message.getReceiver_id())
                        .document(message.getSender_id());

               //asynchronously write data
                ApiFuture<WriteResult> resultSender = docSender.set(data);
                ApiFuture<WriteResult> resultReceiver = docReceiver.set(data);

        *************************************************************************/


    }

}
