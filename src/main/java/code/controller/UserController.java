package code.controller;

import code.firebase.FirebaseSnippets;
import code.model.User;
import com.google.api.core.ApiFuture;
import com.google.cloud.firestore.*;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.concurrent.ExecutionException;

@RestController
public class UserController {

    private Firestore db = FirebaseSnippets.db;
    private User user = new User();

    @RequestMapping("/") //For handling any request type
    public String index() {
        return "Welcome to API for Chatt app! \n GitLab: @Andrea-code";
    }

    @GetMapping("/user/{id}")
    public User getUserById(@PathVariable String id) throws ExecutionException, InterruptedException {
        DocumentReference docRef = db.collection("users").document(id);

        ApiFuture<DocumentSnapshot> future = docRef.get();
        DocumentSnapshot document = future.get();
        if (document.exists()) {


            if (document.getData().get("profile_photo") != null) {
                user.setProfile_photo(document.getData().get("profile_photo").toString());
            }
            user.setName(document.getData().get("name").toString());
            user.setEmail(document.getData().get("email").toString());
            user.setDescription(document.getData().get("description").toString());
        }
        return user;
    }


    @GetMapping("/users")
    public List<User> getAllUsers() throws ExecutionException, InterruptedException {
        List<User> users = new ArrayList<>();

        // asynchronously retrieve all users
        ApiFuture<QuerySnapshot> query = db.collection("users").get();

        // query.get() blocks on response
        QuerySnapshot querySnapshot = query.get();
        List<QueryDocumentSnapshot> documents = querySnapshot.getDocuments();

        for (QueryDocumentSnapshot document : documents) {
            User user = document.toObject(User.class);
            users.add(user);
        }
        return users;
    }

    @PostMapping("/user")
    public void newUser(@RequestBody User user) throws ExecutionException, InterruptedException {

        DocumentReference docRef = db.collection("users").document(user.getUid());

        // Add document data
        Map<String, Object> data = new HashMap<>();
        data.put("uid", user.getUid());
        data.put("name", user.getName());
        data.put("email", user.getEmail());
        System.out.println("description " + user.getDescription());
        data.put("description", user.getDescription());
        System.out.println("url " + user.getProfile_photo());
        data.put("profile_photo", user.getProfile_photo());

        //asynchronously write data
        ApiFuture<WriteResult> result = docRef.set(data);

        // result.get() blocks on response
        System.out.println("\n Update time : " + result.get().getUpdateTime().toDate() + "\n");
    }

}
