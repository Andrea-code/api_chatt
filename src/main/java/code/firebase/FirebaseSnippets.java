package code.firebase;

import com.google.auth.oauth2.GoogleCredentials;
import com.google.cloud.firestore.Firestore;

import com.google.cloud.firestore.FirestoreOptions;
import com.google.firebase.FirebaseApp;
import com.google.firebase.FirebaseOptions;
import com.google.firebase.cloud.FirestoreClient;

import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;

public class FirebaseSnippets {

    public static Firestore db = FirebaseSnippets.initializeWithServiceAccount();

    public static Firestore initializeWithServiceAccount() {
        try {
            // Use a service account
            InputStream serviceAccount = new FileInputStream("HERE GOES YOUT JSON FILE");
            GoogleCredentials credentials = GoogleCredentials.fromStream(serviceAccount);

            FirebaseOptions options = new FirebaseOptions.Builder()
                    .setCredentials(credentials)
                    .setFirestoreOptions(FirestoreOptions.newBuilder()
                            .setTimestampsInSnapshotsEnabled(true)
                            .build())
                    .build();

            FirebaseApp.initializeApp(options);

        }catch (FileNotFoundException e) { e.printStackTrace(); }
         catch (IOException e) { e.printStackTrace(); }

        return FirestoreClient.getFirestore();
    }


}
